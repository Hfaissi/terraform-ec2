TERRAFORM-TRAINING
TP-2 : Déployez votre Ressource AWS avec terraform

Introduction:

Terraform est un outil open source qui permet de gérer l'infrastructure cloud de manière déclarative. Il permet de créer, modifier et détruire des ressources dans le cloud en utilisant un langage de configuration simple.

Détails du projet:

Dans ce projet, nous allons apprendre à déployer une ressource AWS à l'aide de Terraform. Nous allons utiliser une instance Amazon EC2 comme exemple.

Objectifs du projet:

À la fin de ce projet, vous serez en mesure de :

- Comprendre le concept de l'Infrastructure As Code (IaC)
- Installer et configurer Terraform
- Écrire un code Terraform pour déployer une instance EC2
- Déployer une instance EC2 dans le cloud


Les étapes:
- Récupérez le secret et access key de votre compte (dans les paramètres sécurité de votre compte dans IAM)
- Créez un paire de clé dans EC2 et nommez cette clé devops, un fichier devops.pem sera téléchargé
- Créez une fichier ec2.tf 

Renseignez les informations permettant de créer une VM avec l’image d'Amazon.

 (nous travaillerons uniquement dans la region US East (Ohio)us-east-2 dans toute cette formation)

Vérifiez que votre instance est bien créée et observez le contenu de fichier tfstate.

Modifiez le fichier ec2.tf afin d’y inclure le tag de votre instance : “Name: ec2-”.

Appliquez la modification et constatez les changements apportées ainsi que dans le fichier tfstate.
[Résultat de la commande terraform-init] 

[Résultat de la commande terraform-plan] 

[Résultat de la commande terraform-apply] 
**Supprimez votre ec2 **
[Résultat de la commande terraform-destroy] 
